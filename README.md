# Software Challenge
This repository is part of a particular technical assesment. This project is primarily done in Python 3, denoted by containing "`#!/usr/bin/env python3`" in the relevant ROS nodes. 

<hr>

## 1) Data Structures

Relevant Files (Python):
```
software_challenge/
└── software_challenge_py/
    └── card_game_helpers.py
```

Relevant Files (C++):
```
software_challenge/
└── src/
    └── card_game_helpers.cpp
└── include/
    └──software_challenge/
        └── card_game_helpers.hpp
```


<hr>

## 2) Unit Testing

Relevant Files (Python):
```
software_challenge/
└── software_challenge_py/
    └── test_card_game_helpers.py
```

Relevant Files (C++, not fully implmented):
```
software_challenge/
└── tests/
    └── test_deck.cpp
```

To run the tests (for Python unittests):
```bash
cd path/to/repo/software_challenge/software_challenge/software_challenge_py/
python -m unittest discover -v
```

Which should display the following results if all tests pass:
```bash 
test_add (tests.test_card_game_helpers.TestDeck) ... ok
test_cut (tests.test_card_game_helpers.TestDeck) ... ok
test_draw_from_top (tests.test_card_game_helpers.TestDeck) ... ok
test_shuffle (tests.test_card_game_helpers.TestDeck) ... ok

---------------------
Ran 4 tests in 0.000s

OK
```

<hr>

## 3) ROS2 App 

To run the relevant Python nodes in seperate terminals:
```
ros2 run software_challenge player.py
ros2 run software_challenge dealer.py
```

Or the corresponding C++ nodes:
```
ros2 run software_challenge player
ros2 run software_challenge dealer
```

<hr>

## 4) ROS2 Communication

To run:
```bash
ros2 launch software_challenge node_a_b_py.launch.py 
```

Output:
```bash
...
[node_b]: Received request to publish a number.
[node_b]: Publishing the number 8 every 1 second(s).
[node_a]: Node B responded with Publishing number: 8
[node_a]: Received 8 from Node B.
[node_a]: Received 8 from Node B.
...
```

Note: Custom ROS2 interfaces used for this portion of the challenge can be found at:
```
software_challenge_msgs/
└── srv/
    └── NumberToPublish.srv
```

The number which gets published can be changes via parameter of `node_a`. You can either modify the `parameters` argument of the Node object in the launch file at `software_challenge/software_challenge_py/node_a_b_py.launch.py` or by passing ros arguments to the node if manually running with `ros2 run` 
```bash
ros2 run software_challenge node_b.py
```

```bash
ros2 run software_challenge node_a.py --ros-args -p number_to_publish:=5
```
Note: node_b will only process one request on which number to publish. Also node_b is configured to publish integers only. This part of the project is only done in Python. 

<hr>

## 5) Containers
Refer to the `Dockerfile` for more information. To build and use docker container environment, do the following:
```bash
cd /path/to/this/repo/
docker build -t software_challenge .
```

Then you can run the container, which be default will run the `dealer_player.launch.py` file: 
```bash
docker container run --rm -it --name software_challenge_container software_challenge
```

which will launch the dealer and player nodes, outputting something similar to the following: 
```bash
[dealer_node]: Dealer node is ready to deal cards
[player_node]: [  |2♣|  |10♠|  |2♦|  |3♥|  |6♠|  ]
```

You can stop the container by hiting Ctrl + C, or by running the following in a seperate terminal: 
```bash
docker container stop software_challenge_container
```

If you want to explore the container, you can instead launch a bash instance 
```bash
docker container run --rm -it --name software_challenge_container software_challenge bash
```

Once in the bash instance of the container, you can source the `software_challenge_entrypoint.sh` script to get easy access to ROS2 commands and the custom ROS2 package.
```bash
source /software_challenge_entrypoint.sh
```

<hr>

## 6) Continuous Integration (CI)
Refer to the `.gitlab-ci.yml` file. You can see results of the CI tests under the [`CI/CD`] --> [`Pipelines`] tab of this repo.

<hr>

## 7) Cotinuous Delivery (CD)
Refer to the `.gitlab-ci.yml` file, specifically the docker build stage. This will build the docker image and tag it with the commit ID, and the `latest` tag. 

You can find available containers under the [`Packages and registries`] --> [`Container Registry`] tab of this repo. To pull this container, use the following container:
```bash
docker pull registry.gitlab.com/raymond.g.andrade/software_challenge/software_challenge:latest
```

<hr>

## 8) System Design 
Simple implmentation of five-card stud, which a dealer node deals cards to 3 player nodes, and the 3 players display their cards. This portion of the project is only done in Python.

Note: Custom ROS2 interfaces used for this portion of the challenge can be found at:
```
software_challenge_msgs/
└── msg/
    └── Card.msg
└── srv/
    └── DealNumCards.srv
```

To run in context of the docker container, change the default command to run
```bash
docker container run --rm -it --name software_challenge_container software_challenge /five_card_stud.sh
```

Which will show a output similar to the following:
```bash
[dealer_node]: Dealer node is ready to deal cards
[fcs_player_1_node]: [  |5♣|  |K♣|  |8♠|  |3♥|  |J♥|  ]
[fcs_player_2_node]: [  |6♦|  |2♣|  |10♣|  |5♠|  |K♠|  ]
[fcs_player_3_node]: [  |8♥|  |3♦|  |J♦|  |7♣|  |2♠|  ]
```
