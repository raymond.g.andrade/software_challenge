#!/bin/bash

# Source ROS environment
source /ros_entrypoint.sh

# Source your workspace
source /ros_ws/install/setup.bash

# Run the provided command
exec "$@"