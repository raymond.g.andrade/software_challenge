ARG ROS_DISTRO=foxy
FROM osrf/ros:${ROS_DISTRO}-desktop

RUN apt update && apt upgrade -y

RUN apt install -y ros-dev-tools

# Setup the ROS workspace
RUN mkdir -p /ros_ws/src
COPY ./software_challenge /ros_ws/src/software_challenge
COPY ./software_challenge_msgs /ros_ws/src/software_challenge_msgs

# Build the ROS workspace
SHELL ["/bin/bash", "-c"]
RUN cd /ros_ws && \
    source /opt/ros/${ROS_DISTRO}/setup.sh && \
    rosdep update && \
    rosdep install --from-paths src --ignore-src -r -y && \
    colcon build --symlink-install

# Copy the entrypoint and startup scripts
COPY software_challenge_entrypoint.sh /
COPY software_challenge_start.sh /
COPY five_card_stud.sh /

# Ensure python and bash scripts are executable 
RUN chmod +x /software_challenge_entrypoint.sh
RUN chmod +x /software_challenge_start.sh
RUN chmod +x /five_card_stud.sh
RUN find /ros_ws/src/software_challenge/software_challenge_py -type f -name "*.py" -exec chmod +x {} \;

ENTRYPOINT [ "/software_challenge_entrypoint.sh" ]

CMD [ "/software_challenge_start.sh" ]