#include <gtest/gtest.h>
#include "software_challenge/card_game_helpers.hpp"

bool compareCards(const Card& card1, const Card& card2) {
    return card1.suit == card2.suit && card1.rank == card2.rank;
}

class DeckTest : public ::testing::Test {
protected:
    void SetUp() override {
        
    }

    void TearDown() override {
        
    }
};


TEST_F(DeckTest, AddTest) {
    Deck deck;
    size_t initial_length = deck.getCards().size();
    deck.add(Card(CardSuit::SPADES, CardRank::ACE));
    EXPECT_EQ(deck.getCards().size(), initial_length + 1);
}


TEST_F(DeckTest, DrawFromTopTest) {
    Deck deck;
    EXPECT_EQ(deck.getCards().size(), 0);
    deck.add(Card(CardSuit::SPADES, CardRank::ACE));
    deck.add(Card(CardSuit::HEARTS, CardRank::TWO));
    size_t initial_length = deck.getCards().size();
    EXPECT_EQ(initial_length, 2);

    Card drawn_card = deck.drawFromTop();
    // EXPECT_EQ(drawn_card, Card(CardSuit::SPADES, CardRank::ACE));
    EXPECT_TRUE(compareCards(drawn_card, Card(CardSuit::SPADES, CardRank::ACE)));
    EXPECT_EQ(deck.getCards().size(), initial_length - 1);
}


TEST_F(DeckTest, CutTest) {
    Deck deck;
    for (int i = 0; i < 10; i++) {
        deck.add(Card(CardSuit::SPADES, CardRank(i + 1)));
    }

    deck.cut();
    for (int i : {6, 7, 8, 9, 10, 1, 2, 3, 4, 5}) {
        // EXPECT_EQ(deck.drawFromTop(), Card(CardSuit::SPADES, CardRank(i)));
        EXPECT_TRUE(compareCards(deck.drawFromTop(), Card(CardSuit::SPADES, CardRank(i))));
    }
}


TEST_F(DeckTest, ShuffleTest) {
    Deck deck;
    for (int i = 0; i < 10; i++) {
        deck.add(Card(CardSuit::SPADES, CardRank(i + 1)));
    }

    deck.shuffle();
    for (int i : {1, 6, 2, 7, 3, 8, 4, 9, 5, 10}) {
        // EXPECT_EQ(deck.drawFromTop(), Card(CardSuit::SPADES, CardRank(i)));
        EXPECT_TRUE(compareCards(deck.drawFromTop(), Card(CardSuit::SPADES, CardRank(i))));
    }
}

// Run the tests
int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}