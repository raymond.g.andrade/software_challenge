#ifndef DEALER_HPP
#define DEALER_HPP

#include "rclcpp/rclcpp.hpp"
#include "software_challenge_msgs/msg/card.hpp"
#include "software_challenge_msgs/srv/deal_num_cards.hpp"

#include "card_game_helpers.hpp"

class DealerNode : public rclcpp::Node
{
public:
    DealerNode();

private:
    void addStandard52CardDeck();
    void standardShuffle(int num_shuffles=5);
    void drawXCardHand(int x=5);

    void dealCardCallback(
        const std::shared_ptr<software_challenge_msgs::srv::DealNumCards::Request> request,
        std::shared_ptr<software_challenge_msgs::srv::DealNumCards::Response> response);

    Deck deck;
    rclcpp::Service<software_challenge_msgs::srv::DealNumCards>::SharedPtr srv;
};

#endif 