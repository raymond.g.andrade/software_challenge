#ifndef SOFTWARE_CHALLENGE_PLAYER_HPP
#define SOFTWARE_CHALLENGE_PLAYER_HPP

#include "rclcpp/rclcpp.hpp"
#include "software_challenge/card_game_helpers.hpp"

#include <random>

class PlayerNode : public rclcpp::Node {
public:
    PlayerNode();
    void print_hand();
    void pretty_print_hand();

private:
    void add_x_random_cards_to_hand(int x = 5);

    Deck hand;
    std::random_device rd;
};

#endif  // SOFTWARE_CHALLENGE_PLAYER_HPP