#pragma once

#include <string>
#include <vector>
#include <algorithm>
#include "software_challenge_msgs/msg/card.hpp"

enum class CardSuit {
    CLUBS = 1,
    SPADES = 2,
    HEARTS = 3,
    DIAMONDS = 4,
    INVALID
};

enum class CardRank {
    ACE = 1,
    TWO = 2,
    THREE = 3,
    FOUR = 4,
    FIVE = 5,
    SIX = 6,
    SEVEN = 7,
    EIGHT = 8,
    NINE = 9,
    TEN = 10,
    JACK = 11,
    QUEEN = 12,
    KING = 13,
    INVALID
};

struct Card {
    CardSuit suit;
    CardRank rank;
    Card(CardSuit suit, CardRank rank) : suit(suit), rank(rank) {}
    std::string toString() const;
    software_challenge_msgs::msg::Card toRosMsg() const;
};

class Deck {
private:
    std::vector<Card> cards;
public:
    void add(const Card& card);
    Card drawFromTop();
    void cut();
    void shuffle();
    std::vector<Card>& getCards();
};