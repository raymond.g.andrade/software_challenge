#!/usr/bin/env python3
import sys 
sys.path.append("..")
from software_challenge_py.card_game_helpers import Card, CardSuit, CardRank, Deck

from software_challenge_msgs.msg import Card as CardMsg
from software_challenge_msgs.srv import DealNumCards

import rclpy 
from rclpy.node import Node

class DealerNode(Node):
    def __init__(self):
        super().__init__("dealer_node")
        self.deck = Deck()
        self._add_standard_52_card_deck()
        self._standard_shuffle()
        self.deck.cut()
        # self.draw_x_card_hand(5)

        # self.get_logger().info("Top 20 cards in deck: " + str([str(card) for card in self.deck.cards[:20]]))
        self.srv = self.create_service(DealNumCards, 'deal_cards', self.deal_card_callback)
        self.get_logger().info("Dealer node is ready to deal cards")
        

    def _add_standard_52_card_deck(self):
        for suit in CardSuit:
            for rank in CardRank:
                self.deck.add(Card(suit, rank))

    def _standard_shuffle(self, num_shuffles:int=5):
        for i in range(num_shuffles):
            self.deck.shuffle()

    def draw_x_card_hand(self, x:int=5):
        for i in range(x):
            self.deck.draw_from_top()

    def deal_card_callback(self, request, response):
        response.cards = []
        for i in range(request.num_cards_to_deal):
            response.cards.append(self.deck.draw_from_top().to_ros_msg())
        return response
        



if __name__ == "__main__":
    rclpy.init()
    node = DealerNode()

    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        pass
    except Exception as e:
        print(e)
    
    try: 
        node.destroy_node()
    except:
        pass 

