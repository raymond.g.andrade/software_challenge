#!/usr/bin/env python3
import os
import sys 
sys.path.append("..")
from software_challenge_py.card_game_helpers import Card, CardSuit, CardRank, Deck

from software_challenge_msgs.srv import DealNumCards

import rclpy 
from rclpy.node import Node

class FiveCardStudPlayerNode(Node):
    def __init__(self):
        super().__init__("player_node")
        self.hand = []
        self.request_cards()

    def request_cards(self):
        client = self.create_client(DealNumCards, 'deal_cards')
        # while not client.wait_for_service(timeout_sec=1.0):
        #     self.get_logger().info('service not available, waiting again...')
        if client.wait_for_service(timeout_sec=5.0) == False:
            self.get_logger().warn('deal_cards service not available, exiting node...')
            # self.destroy_node()
            ## throw exception instead of exiting node
            raise Exception('deal_cards service not available, exiting node...')

        request = DealNumCards.Request()
        request.num_cards_to_deal = 5
        future = client.call_async(request)
        rclpy.spin_until_future_complete(self, future)
        if future.result() is not None:
            for card_msg in future.result().cards:
                self.hand.append(Card(card_msg.suit, card_msg.rank))
        else:
            self.get_logger().info('Service call failed %r' % (future.exception(),))

    def print_hand(self):
        # Use ROS logger instead of print() since print() does not show up in launch files
        self.get_logger().info(str(self.hand))

    def pretty_print_hand(self):
        print_str = '[  '
        for card in self.hand:
            print_str += str(card) + '  '
        print_str += ']'

        self.get_logger().info(print_str)




if __name__ == "__main__":
    rclpy.init()
    node = FiveCardStudPlayerNode()
    node.pretty_print_hand()

    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        pass
    except Exception as e:
        print(e)
    
    try: 
        node.destroy_node()
    except:
        pass

