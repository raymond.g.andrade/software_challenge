#!/usr/bin/env python3

import sys 
sys.path.append("..")
from software_challenge_py.card_game_helpers import Card, CardSuit, CardRank, Deck

import rclpy 
from rclpy.node import Node

import random

class PlayerNode(Node):
    def __init__(self):
        super().__init__("player_node")
        self.hand = Deck()
        self._add_x_random_cards_to_hand(5)

    def _add_x_random_cards_to_hand(self, x:int=5):
        for i in range(x):
            self.hand.add(Card(random.choice(list(CardSuit)), random.choice(list(CardRank))))

    def print_hand(self):
        # Use ROS logger instead of print() since print() does not show up in launch files
        self.get_logger().info(str(self.hand.cards))

    def pretty_print_hand(self):
        print_str = '[  '
        for card in self.hand.cards:
            print_str += str(card) + '  '
        print_str += ']'

        self.get_logger().info(print_str)




if __name__ == "__main__":
    rclpy.init()
    node = PlayerNode()
    node.pretty_print_hand()

    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        pass
    except Exception as e:
        print(e)
    
    try: 
        node.destroy_node()
    except:
        pass

