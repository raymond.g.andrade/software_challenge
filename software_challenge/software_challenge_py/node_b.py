#!/usr/bin/env python3

import rclpy 
from rclpy.node import Node

from std_msgs.msg import Int32
from software_challenge_msgs.srv import NumberToPublish

class NodeB(Node):
    def __init__(self):
        super().__init__("node_b")
        self.get_logger().info("Node B has been started.")
        self.number_publisher = self.create_publisher(Int32, 'number_topic', 1)
        self.create_service(NumberToPublish, 'publish_number', self.publish_number_callback)
        self.number_publisher_timer = None

    def publish_number_callback(self, request, response):
        self.get_logger().info("Received request to publish a number.")
       
        msg = Int32()
        msg.data = request.number_to_publish
       
        if self.number_publisher_timer:
            response.success = False
            response.message = f"Already Publishing number: {msg.data}, request ignored."
        else:
            self.start_publishing_number(msg)
            response.success = True
            response.message = f"Publishing number: {msg.data}"
        
        return response
    
    def start_publishing_number(self, msg):
        pub_rate_sec = 1
        self.get_logger().info(f"Publishing the number {msg.data} every {pub_rate_sec} second(s).")
        self.number_publisher_timer = self.create_timer(pub_rate_sec, lambda: self.number_publisher.publish(msg))


if __name__ == "__main__":
    rclpy.init()
    node = NodeB()
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        pass
    except Exception as e:
        print(e)
    
    try: 
        node.destroy_node()
    except:
        pass