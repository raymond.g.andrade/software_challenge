#!/usr/bin/env python3

import rclpy 
from rclpy.node import Node

from std_msgs.msg import Int32
from software_challenge_msgs.srv import NumberToPublish

class NodeA(Node):
    def __init__(self):
        super().__init__("node_a")
        self.get_logger().info("Node A has been started.")
        self.declare_parameter("number_to_publish", 10)
        self.create_subscription(Int32, 'number_topic', self.num_received_callback, 1)
        self.number_to_pub_client = self.create_client(NumberToPublish, 'publish_number')
        self.req_number_to_be_published()

    def num_received_callback(self, msg):
        self.get_logger().info(f"Received {msg.data} from Node B.")


    def req_number_to_be_published(self):
        while not self.number_to_pub_client.wait_for_service(timeout_sec=1.0):
            self.get_logger().warn("Waiting for Node B to come up...")
        self.get_logger().info("Node B is up.")
        req = NumberToPublish.Request()
        # req.number_to_publish = 8
        req.number_to_publish = int(self.get_parameter("number_to_publish").value)
        future = self.number_to_pub_client.call_async(req)
        future.add_done_callback(self.req_number_to_be_published_callback)

    def req_number_to_be_published_callback(self, future):
        try:
            response = future.result()
            self.get_logger().info(f"Node B responded with {response.message}")
        except Exception as e:
            self.get_logger().error(f"Service call failed: {e}")



if __name__ == "__main__":
    rclpy.init()
    node = NodeA()
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        pass
    except Exception as e:
        print(e)
    
    try: 
        node.destroy_node()
    except:
        pass

    