import os
import sys
import unittest

sys.path.append("..") 
from software_challenge_py.card_game_helpers import Card, CardSuit, CardRank, Deck

class TestDeck(unittest.TestCase):
    def test_add(self):
        deck = Deck()
        initial_length = len(deck.cards)
        deck.add(Card(CardSuit.SPADES, CardRank.ACE))
        self.assertEqual(len(deck.cards), initial_length + 1)

    def test_draw_from_top(self):
        deck = Deck()
        self.assertEqual(len(deck.cards), 0)
        deck.add(Card(CardSuit.SPADES, CardRank.ACE))
        deck.add(Card(CardSuit.HEARTS, CardRank.TWO))
        inital_length = len(deck.cards)
        self.assertEqual(inital_length, 2)

        drawn_card = deck.draw_from_top()
        self.assertEqual(drawn_card, Card(CardSuit.SPADES, CardRank.ACE))
        self.assertEqual(len(deck.cards), inital_length - 1)

    def test_cut(self):
        deck = Deck()
        
        i = 0
        while i < 10:
            deck.add(Card(CardSuit.SPADES, CardRank(i+1)))
            i+=1

        deck.cut()
        for i in [6, 7, 8, 9, 10, 1, 2, 3, 4, 5]:
            self.assertEqual(deck.draw_from_top(), Card(CardSuit.SPADES, CardRank(i)))

    def test_shuffle(self):
        deck = Deck()
        
        i = 0
        while i < 10:
            deck.add(Card(CardSuit.SPADES, CardRank(i+1)))
            i+=1

        deck.shuffle()
        for i in [1, 6, 2, 7, 3, 8, 4, 9, 5, 10]:
            self.assertEqual(deck.draw_from_top(), Card(CardSuit.SPADES, CardRank(i)))

if __name__ == '__main__':
    unittest.main()