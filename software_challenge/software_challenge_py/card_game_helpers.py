from enum import Enum 
from dataclasses import dataclass

from software_challenge_msgs.msg import Card as CardMsg

class CardSuit(Enum):
    CLUBS = 1
    SPADES = 2
    HEARTS = 3
    DIAMONDS = 4

    def __str__(self) -> str:
        values_to_unix_char = {
            CardSuit.SPADES: "♠",
            CardSuit.HEARTS: "♥",
            CardSuit.CLUBS: "♣",
            CardSuit.DIAMONDS: "♦"
        }
        return values_to_unix_char[self]


class CardRank(Enum):
    ACE = 1
    TWO = 2
    THREE = 3
    FOUR = 4
    FIVE = 5 
    SIX = 6 
    SEVEN = 7
    EIGHT = 8
    NINE = 9 
    TEN = 10
    JACK = 11
    QUEEN = 12
    KING = 13

    def __str__(self):
        values_to_map = {
            CardRank.ACE: "A",
            CardRank.JACK: "J",
            CardRank.QUEEN: "Q",
            CardRank.KING: "K"
        }
        if self in values_to_map:
            return values_to_map[self]
        else:
            return str(self.value)


@dataclass
class Card:
    suit: CardSuit
    rank: CardRank

    def __str__(self) -> str:
        return f"|{str(self.rank)}{str(self.suit)}|"
    
    def __post_init__(self):
        if isinstance(self.suit, int):
            self.suit = CardSuit(self.suit)
        if isinstance(self.rank, int):
            self.rank = CardRank(self.rank)

    def to_ros_msg(self):
        return CardMsg(suit=self.suit.value, rank=self.rank.value)


class Deck:
    def __init__(self):
        self.cards = []

    def add(self, card: Card):
        self.cards.append(card)

    def draw_from_top(self):
        return self.cards.pop(0)
    
    def cut(self):
        midpoint_index = len(self.cards) // 2

        # Cut deck in half and reverse each half (prevents having to use intermediate list)
        self.cards[:midpoint_index] = self.cards[:midpoint_index][::-1]
        self.cards[midpoint_index:] = self.cards[midpoint_index:][::-1]
        self.cards.reverse()

    def shuffle(self):
        # split deck into two halves
        # shuffle each half into each other one card at a time
        # [1, 2, 3, 4, 5, 6] --> [1, 2, 3] [4, 5, 6] --> [1, 4, 2, 5, 3, 6]
        midpoint_index = len(self.cards) // 2

        i = 0
        while i < midpoint_index:
            self.cards.insert(i*2+1, self.cards.pop(midpoint_index+i))
            i+=1

