#include "rclcpp/rclcpp.hpp"
#include "software_challenge/dealer.hpp"
#include "software_challenge/card_game_helpers.hpp"
// #include "software_challenge_msgs/msg/card.hpp"

DealerNode::DealerNode() : Node("dealer_node") {
  for(int suit_int = static_cast<int>(CardSuit::CLUBS); suit_int != static_cast<int>(CardSuit::INVALID); suit_int++) {
    for(int rank_int = static_cast<int>(CardRank::ACE); rank_int != static_cast<int>(CardRank::INVALID); rank_int++) {
        deck.add(Card(static_cast<CardSuit>(suit_int), static_cast<CardRank>(rank_int)));
    }
  }

  standardShuffle(5);
  deck.cut();

  srv = this->create_service<software_challenge_msgs::srv::DealNumCards>(
      "deal_cards",
      std::bind(&DealerNode::dealCardCallback, this, std::placeholders::_1, std::placeholders::_2));

  RCLCPP_INFO(this->get_logger(), "Dealer node is ready to deal cards");
  // RCLCPP_INFO(this->get_logger(), "First 5 cards in deck: %s %s %s %s %s",
  //     deck.getCards()[0].toString().c_str(),
  //     deck.getCards()[1].toString().c_str(),
  //     deck.getCards()[2].toString().c_str(),
  //     deck.getCards()[3].toString().c_str(),
  //     deck.getCards()[4].toString().c_str()
  // );
}

void DealerNode::addStandard52CardDeck() {
  for(int suit_int = static_cast<int>(CardSuit::CLUBS); suit_int != static_cast<int>(CardSuit::INVALID); suit_int++) {
    for(int rank_int = static_cast<int>(CardRank::ACE); rank_int != static_cast<int>(CardRank::INVALID); rank_int++) {
        deck.add(Card(static_cast<CardSuit>(suit_int), static_cast<CardRank>(rank_int)));
    }
  }
}

void DealerNode::standardShuffle(int num_shuffles) {
  for (int i = 0; i < num_shuffles; i++)
    deck.shuffle();
}

void DealerNode::drawXCardHand(int x) {
  for (int i = 0; i < x; i++)
      deck.drawFromTop();
}

void DealerNode::dealCardCallback(
  const std::shared_ptr<software_challenge_msgs::srv::DealNumCards::Request> request,
  std::shared_ptr<software_challenge_msgs::srv::DealNumCards::Response> response) {

  for (int i = 0; i < request->num_cards_to_deal; i++) {
    response->cards.push_back(deck.drawFromTop().toRosMsg());
  }
}

int main(int argc, char ** argv) {
  rclcpp::init(argc, argv);
  auto node = std::make_shared<DealerNode>();
  rclcpp::spin(node);
  rclcpp::shutdown();
  return 0;
}