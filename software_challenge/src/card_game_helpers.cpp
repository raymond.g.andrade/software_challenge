#include <random>
#include <vector>
#include <algorithm>
#include "software_challenge/card_game_helpers.hpp"

std::string Card::toString() const {
    std::string suitStr;
    std::string rankStr;

    switch(suit) {
        case CardSuit::CLUBS: suitStr = "♣"; break;
        case CardSuit::SPADES: suitStr = "♠"; break;
        case CardSuit::HEARTS: suitStr = "♥"; break;
        case CardSuit::DIAMONDS: suitStr = "♦"; break;
        case CardSuit::INVALID: suitStr = "INVALID"; break;
        default: suitStr = "?"; break;
    }

    switch(rank) {
        case CardRank::ACE: rankStr = "A"; break;
        case CardRank::TWO: rankStr = "2"; break;
        case CardRank::THREE: rankStr = "3"; break;
        case CardRank::FOUR: rankStr = "4"; break;
        case CardRank::FIVE: rankStr = "5"; break;
        case CardRank::SIX: rankStr = "6"; break;
        case CardRank::SEVEN: rankStr = "7"; break;
        case CardRank::EIGHT: rankStr = "8"; break;
        case CardRank::NINE: rankStr = "9"; break;
        case CardRank::TEN: rankStr = "10"; break;
        case CardRank::JACK: rankStr = "J"; break;
        case CardRank::QUEEN: rankStr = "Q"; break;
        case CardRank::KING: rankStr = "K"; break;
        case CardRank::INVALID: rankStr = "INVALID"; break;
        default: rankStr = "?"; break;
    }

    return "|" + rankStr + suitStr + "|";
}

software_challenge_msgs::msg::Card Card::toRosMsg() const {
    software_challenge_msgs::msg::Card cardMsg;
    cardMsg.suit = static_cast<int>(suit);
    cardMsg.rank = static_cast<int>(rank);
    return cardMsg;
}

void Deck::add(const Card& card) {
    cards.push_back(card);
}

Card Deck::drawFromTop() {
    Card topCard = cards.front();
    cards.erase(cards.begin());
    return topCard;
}

void Deck::cut() {
    size_t midpoint = cards.size() / 2;
    std::reverse(cards.begin(), cards.begin() + midpoint);
    std::reverse(cards.begin() + midpoint, cards.end());
    std::reverse(cards.begin(), cards.end());
}

void Deck::shuffle() {
    int midpoint_index = cards.size() / 2;

    int i = 0;
    while (i < midpoint_index) {
        cards.insert(cards.begin() + i * 2 + 1, cards[midpoint_index + i]);
        cards.erase(cards.begin() + midpoint_index + i + 1);
        i++;
    }
}

std::vector<Card>& Deck::getCards() {
    return cards;
}