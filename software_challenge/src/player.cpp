#include "software_challenge/player.hpp"

PlayerNode::PlayerNode() : Node("player_node") {
    add_x_random_cards_to_hand(5);
}

void PlayerNode::add_x_random_cards_to_hand(int x) {
    std::uniform_int_distribution<int> suit_dist(1, static_cast<int>(CardSuit::INVALID) - 1);
    std::uniform_int_distribution<int> rank_dist(1, static_cast<int>(CardRank::INVALID) - 1);
    std::mt19937 gen(rd());

    for (int i = 0; i < x; i++) {
        hand.add(Card(static_cast<CardSuit>(suit_dist(gen)), static_cast<CardRank>(rank_dist(gen))));
    }
}

void PlayerNode::print_hand() {
    for (const Card& card : hand.getCards()) {
        RCLCPP_INFO(this->get_logger(), card.toString().c_str());
    }
}

void PlayerNode::pretty_print_hand() {
    std::string print_str = "[  ";
    for (const Card& card : hand.getCards()) {
        print_str += card.toString() + "  ";
    }
    print_str += "]";
    RCLCPP_INFO(this->get_logger(), print_str.c_str());
}

int main(int argc, char ** argv) {
    rclcpp::init(argc, argv);
    auto node = std::make_shared<PlayerNode>();

    node->pretty_print_hand();

    try {
        rclcpp::spin(node);
    } catch(const std::exception& e) {
        RCLCPP_ERROR(node->get_logger(), "Exception caught: %s", e.what());
    }

    rclcpp::shutdown();
    return 0;
}