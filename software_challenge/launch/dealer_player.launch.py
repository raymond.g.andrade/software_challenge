from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package="software_challenge",
            executable="dealer.py",
            name="dealer_node"
        ),
        Node(
            package="software_challenge",
            executable="player.py",
            name="player_node"
        ),
    ])
