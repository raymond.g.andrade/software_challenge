from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package="software_challenge",
            executable="dealer.py",
            name="dealer_node"
        ),
        Node(
            package="software_challenge",
            executable="five_card_stud_player.py",
            name="fcs_player_1_node"
        ),
        Node(
            package="software_challenge",
            executable="five_card_stud_player.py",
            name="fcs_player_2_node"
        ),
        Node(
            package="software_challenge",
            executable="five_card_stud_player.py",
            name="fcs_player_3_node"
        ),
    ])
