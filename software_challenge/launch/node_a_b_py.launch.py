from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package="software_challenge",
            executable="node_a.py",
            name="node_a",
            parameters=[
                {"number_to_publish": 8}
            ]
        ),
        Node(
            package="software_challenge",
            executable="node_b.py",
            name="node_b"
        )
    ])